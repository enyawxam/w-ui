import './DropActions.less';
//
import React from 'react';
import {useState, useEffect, useRef} from 'react';
import useOnClickOutside from '../../../hooks/onClickOutside';


export default function DropActions(props) {
  const [isOpened, open] = useState(false);
  const DropActionsRef = useRef();

  useOnClickOutside(DropActionsRef, () => open(false));

  function ActionItem(item) {
    return <>
      <div className="DropActions__ActionItem no_select" onClick={() => {
        open(false);
        item.onClick();
      }}>
        <span>
          {item.title}
        </span>
      </div>
    </>
  }

  return !isOpened ? <div className="DropActions__openOverlay" onClick={() => open(true)}>
  </div> : <>
    <div ref={DropActionsRef} className="DropActions__main no_select"
         style={{width: props.width, top: props.top, left: props.left, right: props.right}}
    /**/>
      {props.items.length > 0 ? props.items.map((item, key) => {
        return <div key={key}>
          {ActionItem(item)}
        </div>
      }): props.content}
    </div>
  </>
}

DropActions.defaultProps = {
  width: 'auto',
  top: 'inherit',
  left: 'inherit',
  right: 'inherit',
  items: [],
  content: <></>
};