import './input.less';

import React  from 'react';
import classNames from 'classnames';

class Input extends React.Component {
  constructor(props) {
    super(props);
    this.input = React.createRef();
  }
  render() {
    const props = {...this.props};
    const Input__wrapper = 'Input__wrapper';
    const InputWrapper = props.classNameWrapper ? Input__wrapper + ' ' + props.classNameWrapper : Input__wrapper;

    let params = {
      className: classNames({Input: true, error: props.error}),
      placeholder: props.placeholder,
      type: props.type,
      autoComplete: props.autoComplete,
      autoFocus: props.autoFocus,
      onKeyPress: props.onKeyPress,
      readOnly: props.readOnly,
      onFocus: props.onFocus,
      required: true,
    };

    return (
      <div className={InputWrapper} onClick={props.onClick}>
        <input
          ref="input"
          {...params}
          value={props.value}
          onChange={this.__onChange}
          disabled={props.disabled}
          autoFocus={props.autoFocus}
        />
      </div>
    )
  }

  __onChange = e => {
    const props = {...this.props};
    props.onChange(e);
    props.onTextChange(e.target.value);
  };

  focus() {
    this.input.focus();
  }
}

Input.defaultProps = {
  classNameWrapper: '',
  disabled: false,
  error: false,
  autoFocus: false,
  onClick: e => {},
  onChange: e => {},
  onTextChange: e => {}
};

export default React.memo(Input);
