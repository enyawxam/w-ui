import './SecondaryInput.less';
//
import React, {Component} from 'react';
//
import Input from '../Input';
import TextareaAutosize from 'react-textarea-autosize';
import classNames from 'classnames';

class SecondaryInput extends Component {
  constructor(props) {
    super(props);
    this.secondaryInput = React.createRef();
  }

  state = {
    visiblePassword: false
  };

  __focus = () => {
    if (this.props.typeInput === 'input') {
      this.secondaryInput.current.refs.input.focus();
    } else if (this.props.typeInput === 'textarea') {
      this.secondaryInput.focus()
    }
  };

  __visiblePasswordToggle = () => {
    this.setState({visiblePassword: !this.state.visiblePassword});
  };

  __getWidth = () => {
    return (this.props.type === 'password' && this.props.typeInput === 'input') ? 'calc(100% - 50px)' : '100%';
  };

  render() {
    const {props} = this;
    return <div className="SecondaryInput__main">
      <div className="SecondaryInput__wrapper" onClick={this.__focus}>
        {props.title && <div className="SecondaryInput__title no_select">
          {props.title}
        </div>}
        <div className="SecondaryInput__InputWrapper" style={
          {width: this.__getWidth()}
        }>
          {props.typeInput === 'input' && <Input
            ref={this.secondaryInput}
            {...props}
            type={props.type !== 'password' ? props.type : (this.state.visiblePassword === false ? 'password' : 'text')}
          />}
          {props.typeInput === 'textarea' && <TextareaAutosize
            placeholder={props.placeholder}
            className="SecondaryInput__textarea"
            maxRows={props.maxRows}
            minRows={props.minRows}
            defaultValue={props.value}
            onChange={props.onChange}
            inputRef={tag => (this.secondaryInput = tag)}
          />}
        </div>
        {props.description && <div className="SecondaryInput__description no_select" style={{width: this.__getWidth()}}>
          <div className="hr"> </div>
          {props.description}
        </div>}
      </div>
      {props.typeInput === 'input' && props.type === 'password' && props.value.length > 0 && <div className="SecondaryInput__actionRightIcon" onClick={() => this.__visiblePasswordToggle()}>
        <div className={classNames({'see_image': true, 'not': !this.state.visiblePassword})}> </div>
      </div>}
    </div>
  }
}

SecondaryInput.defaultProps = {
  typeInput: 'input', // or textarea
  type: 'text',
  title: undefined,
  placeholder: 'placeholder',
  maxRows: 10,
  minRows: undefined,
  value: '',
  onChange: () => {}
};

export default SecondaryInput;