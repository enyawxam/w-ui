// styles
import '../index.less';
import './Button.less';
// external
import React from 'react';
import SVG from 'react-inlinesvg';
// internal
import classNames from '../../../utils/classnames';

function Index(props) {
  const className = classNames({
    [props.className]: !!props.className,
    Button: true,
    [props.size]: !!props.size,
    disabled: props.disabled || props.state === 'disabled',
    [props.type]: !!props.type,
    [props.newClass]: !!props.newClass,
    rounded: props.rounded,
    [props.state]: !!props.state
  });

  return (
    <button
      className={className}
      onClick={e => props.onClick && props.onClick(e)}
      style={props.style}
      type={props.btnType}
      title={props.title}
    /**/>
      {props.state === 'loading' && <div className="Button__loader">
        <SVG src={require('./assets/loading.svg')} />
      </div>}
      <div className="Button__cont">
        {props.beforeContent}
        <div className="Button__label" style={props.fontSize ? {fontSize: props.fontSize} : {}}>{props.children}</div>
        {props.afterContent}
      </div>
      {(props.type === 'outline' || props.type === 'negative_outline') && <div className="Button__outline_helper" />}
    </button>
  )
}

export default React.memo(Index);