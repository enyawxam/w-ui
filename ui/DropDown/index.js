import './assets/index.css'
//
import React, {Component}  from 'react'
import classNames from 'classnames';
//
import {ReactComponent as IconTick} from './assets/icons/tick.svg';
import {ReactComponent as IconClose} from './assets/icons/close.svg';
import {ReactComponent as IconAdd} from './assets/icons/add.svg';
import {ReactComponent as IconUp} from './assets/icons/up.svg';
import {ReactComponent as IconDown} from './assets/icons/down.svg';

class DropDown extends Component {
  state = {
    search: '',
    selected: new Map(),
    resultIsDisplayed: false,
    buttonAddIsDisplayed: false,
    mouseOverFlag: false
  };

  componentWillMount() {
    this.input = React.createRef();
    this.boxResult = React.createRef();
  }

  componentDidMount() {
    const preloadmap = this.props.preloadmapFunction(this.props.items);
    if (preloadmap.length > 0) {
      preloadmap.map(item => {
        if (this.__multiSelectNotAllow()) this.__selectedStoreAdd(item.id, item);
      })
    }
    window.addEventListener("click", this.__windowClickHandler);
  }

  componentWillUnmount() {
    window.removeEventListener("click", this.__windowClickHandler);
  }

  get items() {
    return this.props.items.slice().filter(
      ({name}) => (name = name + '') && name.toLowerCase().indexOf(this.state.search.toLowerCase()) > -1
    );
  }

  __windowClickHandler = () => {
    if (!this.state.mouseOverFlag && this.state.resultIsDisplayed) {
      this.__hideBlockResult();
    }
  };

  __inputKeyListener = (evt) => {
    evt = evt || window.event;
    switch (evt.keyCode) {
      case 27:
        this.__hideBlockResult();
        break;
      case 13:
        //
        break;
      default:
        //
        break;
    }
  };

  // block with results

  __toggleBlockResult = () => {
    this.setState({
      ...this.state,
      resultIsDisplayed: !this.state.resultIsDisplayed
    })
  };

  __hideBlockResult = () => {
    this.setState({
      ...this.state,
      resultIsDisplayed: false
    })
  };

  __showBlockResult = () => {
    this.setState({
      ...this.state,
      resultIsDisplayed: true
    })
  };

  // main block

  __mainParentMouseOverHandler = () => {
    this.setState({
      ...this.state,
      mouseOverFlag: true
    })
  };

  __mainParentMouseOutHandler = () => {
    this.setState({
      ...this.state,
      mouseOverFlag: false
    })
  };

  __mainParentBlurHandler = () => {
    if (!this.state.mouseOverFlag) {
      this.__hideBlockResult();
    }
  };

  __mainParentBlurHelperHandler = (evt) => {
    const allowClass = [
      'DropDown__c_left',
      'DropDown__container_content',
      'DropDown_',
      'DropDown__container'
    ];
    if (allowClass.indexOf(evt.target.classList[0]) > -1) {
      if (this.state.resultIsDisplayed) {
        this.__hideBlockResult();
      } else {
        if (this.__multiSelectNotAllow()) {
          this.__inputFocus();
        } else {
          this.__toggleBlockResult();
        }
      }
    }
  };

  // input

  __inputFocus = () => {
    this.input.current.focus();
  };

  __inputOnChangeValue = (search) => {
    this.setState({
      ...this.state,
      search: search.currentTarget.value
    });
  };

  __inputOnBlurEvent = () => {
    if (!this.state.mouseOverFlag) {
      this.__hideBlockResult();
    }
  };

  // store

  __selectedStoreClear = (cb) => {
    this.setState({
      ...this.state,
      selected: new Map(),
      search: ''
    }, () => {
      this.boxResult.current.scrollTo(0, 0);
      this.__hideBlockResult();
      this.__selectedStoreOnChange();
      if (cb) cb();
    });
  };

  __selectedStoreAdd = (id, value) => {
    this.state.selected.set(id, value);
    this.setState({
      ...this.state,
      selected: this.state.selected,
      search: ""
    }, () => {
      this.boxResult.current.scrollTo(0, 0);
      this.__hideBlockResult();
      this.__selectedStoreOnChange();
    });
  };

  __selectedStoreRemove = (id) => {
    this.state.selected.delete(id);
    this.setState({
      ...this.state,
      selected: this.state.selected
    }, () => {
      this.__selectedStoreOnChange();
    });
  };

  __getSizeSelectedStore = () => {
    return this.state.selected.size;
  };

  __selectedStoreOnChange = () => {
    if (this.props.returnMap) {
      this.props.onChangeCallback(this.state.selected);
    } else {
      const selected = [];
      [...this.state.selected.keys()].forEach(key => {
        selected.push(this.state.selected.get(key));
      });
      this.props.onChangeCallback(selected);
    }
  };

  __multiSelectNotAllow = () => {
    return !(!this.props.multiSelect && this.__getSizeSelectedStore() > 0);
  };

  render() {
    const {props, state} = this;
    return <div
      className={classNames({DropDown_Main: true, NotMultiSelect: !props.multiSelect})}
      onMouseOver={this.__mainParentMouseOverHandler}
      onMouseOut={this.__mainParentMouseOutHandler}
      onBlur={this.__mainParentBlurHandler}
      onClick={this.__mainParentBlurHelperHandler}
    >
      {props.title && <div className="DropDown__Main__title">
        {props.title}
      </div>}
      <div
        className="DropDown__container"
        style={state.resultIsDisplayed ? {borderRadius:"8px 8px 0 0"} : {}}
      >
        <div className="DropDown__container_content">
          <div className="DropDown__c_left">
            <div className="DropDown__storeSelected">
              {[...state.selected.keys()].map((key, i) => {
                const item = state.selected.get(key);
                return <div key={i} className="DropDown__store_item" onClick={() => {!this.__multiSelectNotAllow() && this.__toggleBlockResult()}}>
                  <div className="DropDown__store_item_left">
                    <span>{item.name}</span>
                  </div>
                  <div
                    className="DropDown__store_item_right"
                    onClick={() => this.__selectedStoreRemove(item.id)}
                  >
                    <span>
                      <IconClose />
                    </span>
                  </div>
                </div>
              })}
            </div>
            {this.__multiSelectNotAllow() && <div
                className="DropDown__store_item DropDown__add_button"
                onClick={this.__inputFocus}
                style={state.selected.size > 0 && !state.resultIsDisplayed ? {display : "block"}:{}}
              >
                <div className="DropDown__store_item_left">
                  <span>{props.addButtonText}</span>
                </div>
                <div className="DropDown__store_item_right">
                <span>
                  <IconAdd />
                </span>
                </div>
              </div>
            }
            {this.__multiSelectNotAllow() && <input
              ref={this.input}
              type="text"
              placeholder={props.placeholder}
              onClick={this.__showBlockResult}
              onFocus={this.__showBlockResult}
              onBlur={this.__inputOnBlurEvent}
              onKeyUp={this.__inputKeyListener}
              onChange={this.__inputOnChangeValue}
              value={state.search}
            />}
          </div>
          <div className="DropDown__c_right" onClick={() => {this.__toggleBlockResult()}}>
            <div className="DropDown__svg_vector">
              {state.resultIsDisplayed ? <IconUp/> : <IconDown/>}
            </div>
          </div>
        </div>
        <div className="DropDown__items" style={{display: state.resultIsDisplayed ? "block":"none"}}>
          <div ref={this.boxResult} className="DropDown__result" style={{maxHeight: props.maxResultHeight}}>
            {props.description && <div className="DropDown__Main__description">
              {props.description}
            </div>}
            {this.items.length > 0 ? this.items.map((item, i) => {
              return <div
                key={i}
                className={"DropDown__item" + (state.selected.has(item.id) ? " " + 'DropDown__active' : "")}
                onClick={() => {
                  !state.selected.has(item.id)
                    ? (this.__multiSelectNotAllow() ? this.__selectedStoreAdd(item.id, item) : (this.__selectedStoreClear(() => {
                      this.__selectedStoreAdd(item.id, item)
                    })))
                    : (this.__multiSelectNotAllow() ? this.__selectedStoreRemove(item.id) : this.__toggleBlockResult())
                }}
              >
                {props.withoutPhoto !== true && item.photo &&
                  <div className="DropDown__item_left">
                    <div className="DropDown__item_photo" style={{backgroundImage: "url("+ item.photo +")"}}>
                    </div>
                  </div>
                }
                <div className="DropDown__item_content">
                  <div className="DropDown__name">
                    <span>
                      {item.name}
                      {item.subTitle && <span className="DD_subtitle">
                        {' ' + item.subTitle}
                      </span>}
                    </span>
                  </div>
                  {item.description && <div className="DropDown__description">
                    <span>{item.description}</span>
                  </div>}
                </div>
                <div className="DropDown__item_right">
                  <div className="DropDown__svg_right_item">
                    <IconTick />
                  </div>
                </div>
              </div>
            }) : <div className="DropDown__empty_result" onClick={this.__hideBlockResult}>
              {props.emptyResultText}
            </div>}
          </div>
        </div>
      </div>
    </div>
  }
}

DropDown.defaultProps = {
  maxResultHeight: 203,
  items: [],
  withoutPhoto: false,
  placeholder: 'Start typing something...',
  addButtonText: 'Add',
  emptyResultText: 'Here is that empty...',
  returnMap: false,
  multiSelect: false,
  onChangeCallback: () => {},
  preloadmapFunction: () => {return []},
  description: undefined,
  title: undefined,
};

export default DropDown;