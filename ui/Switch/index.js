import './index.less';
import React  from 'react';
import classNames from 'classnames';

function Switch(props) {
  const className = classNames({
    Switch: true,
    on: props.on,
    disabled: props.disabled
  });

  return (
    <div className={className} onClick={props.onChange}>
      <div className="Switch__control">
        <div className="Switch__indicator" />
      </div>
      <div className="Switch__label">{props.children}</div>
    </div>
  );
}

Switch.defaultProps = {
  on: false,
  onChange: () => {},
  disabled: false
};

export default React.memo(Switch);